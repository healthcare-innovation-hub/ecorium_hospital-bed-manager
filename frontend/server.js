var express = require('express');
var app = express();
var serveStatic = require('serve-static');
var server = require('http').Server(app);

var port = process.env.PORT || 3333;
server.listen(port);
app.use(serveStatic(__dirname + '/dist'));
