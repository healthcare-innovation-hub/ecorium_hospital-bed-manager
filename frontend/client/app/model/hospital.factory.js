(function () {
    'use strict';

    angular.module('app').
        factory('Hospital', function ($q, ParseHelperService, Occupancy) {
            
            var ParseLiveQueryManager = ParseHelperService.CreateLiveQueryManager();

            var model = new Parse.Object.extend('Hospital', {
                // Instance methods

            }, {
                    // Static methods
                    getById : function(objectId) {
                        var defer = $q.defer();

                        var query = new Parse.Query(this);
                        query.equalTo("objectId", objectId);
                        query.find({
                            success : function(list) {
                                defer.resolve(list.length > 0 ? list[0] : null);
                            },
                            error : function(aError) {
                                defer.reject(aError);
                            }
                        });

                        return defer.promise;
                    },

                    listByBounds : function(bounds){
                        var defer = $q.defer();

                        var latitudeWest = bounds.f.b;
                        var latitudeEast = bounds.f.f;

                        var longitudeNorth = bounds.b.f;
                        var longitudeSouth = bounds.b.b;

                        var southwest = new Parse.GeoPoint(latitudeWest, longitudeSouth);
                        var northeast = new Parse.GeoPoint(latitudeEast, longitudeNorth);

                        var query = new Parse.Query(this);
                        query.withinGeoBox("location", southwest, northeast);
                        query.find({
                            success: function (hospitalList) {

                                var promises = [];
                                hospitalList.forEach(function(hospital){

                                    // Get occupancy settings for hospital (grouped by ward type)
                                    hospital.wards = [];
                                    var promise = Occupancy.getByHospital(hospital, 'INTERNAL_MEDICIN').then(function(occupancies){

                                        var newWard = {
                                            wardType:   'INTERNAL_MEDICIN',
                                            occupancies: occupancies 
                                        };

                                        hospital.wards.push(newWard);
                                    });
                                    promises.push(promise);

                                    var promise = Occupancy.getByHospital(hospital, 'SURGERY').then(function(occupancies){
                                        
                                        var newWard = {
                                            wardType:   'SURGERY',
                                            occupancies: occupancies 
                                        };

                                        hospital.wards.push(newWard);
                                    });
                                    promises.push(promise);
                                });

                                // Wait until foreach loop has finished
                                Parse.Promise.when(promises).then(function(){
                                    defer.resolve(hospitalList)
                                });
                            },
                            error: function (error) {
                                defer.reject(error);
                            }
                        });
                        return defer.promise;
                    },

                    list: function () {
                        var defer = $q.defer();
                        var query = new Parse.Query(this);
                        query.find({
                            success: function (hospitalList) {
                                defer.resolve(hospitalList);
                            },
                            error: function (error) {
                                defer.reject(error);
                            }
                        });
                        return defer.promise;
                    },
                });

            ParseHelperService.patchBindings(model, ['name', 'address', 'location']);
            return model;
        });
})();

