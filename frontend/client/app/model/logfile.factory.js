(function () {
    'use strict';

    angular.module('app').
    factory('Logfile',  function($q, AuthService, ParseHelperService) {
;
        var Logfile = Parse.Object.extend("Logfile", {
            // Instance methods

        }, {
            // Rest-API Methods
            list : function() {
                var defer = $q.defer();
                var query = CreateQuery();
                query.include('occupancy');
                query.include('occupancy.hospital');
                query.find({
                    success : function(logfileList) {
                        defer.resolve(logfileList);
                    },

                    error : function(aError) {
                        defer.reject(aError);
                    }
                });

                return defer.promise;
            },

            logChange : function(occupancy){

                var changeLog = new Logfile();
                changeLog.user      = Parse.User.current();
                changeLog.occupancy = occupancy;
                
                changeLog.save();
            },
        });

        function CreateQuery(){
            var query = new Parse.Query(Logfile);
            return query;
        }

        ParseHelperService.patchBindings(Logfile, ['user', 'occupancy']);
        return Logfile;
    });
})();
