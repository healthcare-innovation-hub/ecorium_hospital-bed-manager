(function () {
    'use strict';

    angular.module('app').
        factory('Comment', function ($q, ParseHelperService) {
            
            var ParseLiveQueryManager = ParseHelperService.CreateLiveQueryManager();

            var model = new Parse.Object.extend('Comment', {
                // Instance methods

            }, {
                    // Static methods
                    getById : function(objectId) {
                        var defer = $q.defer();

                        var query = new Parse.Query(this);
                        query.equalTo("objectId", objectId);
                        query.find({
                            success : function(list) {
                                defer.resolve(list.length > 0 ? list[0] : null);
                            },
                            error : function(aError) {
                                defer.reject(aError);
                            }
                        });

                        return defer.promise;
                    },
                });

            ParseHelperService.patchBindings(model, ['user', 'hospital', 'comment']);
            return model;
        });
})();

