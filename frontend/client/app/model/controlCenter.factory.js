(function () {
    'use strict';

    angular.module('app').
        factory('ControlCenter', function ($q, ParseHelperService) {
            //var ParseLiveQueryManager = ParseHelperService.CreateLiveQueryManager();

            var model = new Parse.Object.extend('ControlCenter', {
                // Instance methods

            }, {
                    // Static methods
                    getById : function(objectId) {
                        var defer = $q.defer();

                        var query = new Parse.Query(this);
                        query.equalTo("objectId", objectId);
                        query.find({
                            success : function(list) {
                                defer.resolve(list.length > 0 ? list[0] : null);
                            },
                            error : function(aError) {
                                defer.reject(aError);
                            }
                        });

                        return defer.promise;
                    },

                    list: function () {
                        var defer = $q.defer();
                        var query = new Parse.Query(this);
                        query.find({
                            success: function (areaList) {
                                defer.resolve(areaList);
                            },
                            error: function (error) {
                                defer.reject(error);
                            }
                        });
                        return defer.promise;
                    }
                });

            ParseHelperService.patchBindings(model, ["zip", "faxNumber", "city", "logo", "name", "address", "phoneNumber"]);
            return model;
        });
})();

