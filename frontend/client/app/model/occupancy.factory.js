(function () {
    'use strict';

    angular.module('app').
        factory('Occupancy', function ($q, ParseHelperService) {
            
            var ParseLiveQueryManager = ParseHelperService.CreateLiveQueryManager();

            var model = new Parse.Object.extend('Occupancy', {
                // Instance methods

                }, {

                    // Static methods
                    getByHospital : function(hospital, wardType) {
                        var defer = $q.defer();

                        var query = new Parse.Query(this);
                        query.equalTo("hospital", hospital);
                        
                        if (wardType !== undefined)
                        {
                            query.equalTo("wardType", wardType);
                        }
                        
                        query.ascending("bedType");
                        query.find({
                            success : function(occupancies) {
                                defer.resolve(occupancies);
                            },
                            error : function(aError) {
                                defer.reject(aError);
                            }
                        });

                        return defer.promise;
                    },

                    createDefaults: function(hospital){

                        var defer = $q.defer();

                        var promises = [];
                        model.wardTypes.forEach(function(wardType){
                            
                            model.defaultThresholds.forEach(function(defaultThreshold){

                                var defaultOccupancy = new model;
                                defaultOccupancy.occupancy = defaultThreshold.green.max;
                                defaultOccupancy.wardType  = wardType;
                                defaultOccupancy.bedType   = defaultThreshold.bedType;
                                defaultOccupancy.red       = defaultThreshold.red;
                                defaultOccupancy.yellow    = defaultThreshold.yellow;
                                defaultOccupancy.green     = defaultThreshold.green;
                                defaultOccupancy.hospital  = hospital;

                                var promise = defaultOccupancy.save().then();
                                promises.push(promise);
                            });
                        });  

                        // Wait until foreach loop has finished
                        Parse.Promise.when(promises).then(function(){
                            defer.resolve()
                        }, function(error){
                            defer.reject(error);
                            console.log(error);
                        });

                        return defer.promise;
                    },

                    updateOccupancy: function(occupancy){

                        // Check if input is empty or contains letters
                        if ((occupancy.occupancy.match && !occupancy.occupancy.match(/^\d+$/)) || occupancy.occupancy > occupancy.green.max)
                        {
                            occupancy.occupancy = occupancy.green.max;
                        }

                        if (occupancy.occupancy < 0)
                        {
                            occupancy.occupancy = 0;
                        }

                        // Make sure its an integer
                        occupancy.red.max = parseInt(occupancy.red.max);

                        occupancy.save();
                    },

                    wardTypes: ['INTERNAL_MEDICIN', 'SURGERY'],

                    defaultThresholds: [
                        {
                            bedType: 0,
                            red    : {"min":0,"max":0},
                            yellow : {"min":1,"max":50},
                            green  : {"min":51,"max":100},
                        },
                        {
                            bedType: 1,
                            red    : {"min":0,"max":0},
                            yellow : {"min":1,"max":5},
                            green  : {"min":6,"max":10},
                        },
                        {
                            bedType: 2,
                            red    : {"min":0,"max":0},
                            yellow : {"min":1,"max":1},
                            green  : {"min":2,"max":3},
                        }
                    ],

                    // Live queries
                    SubOccupancy : function(hospital) {
                        var queryID = "SubHospital" + hospital.id;
                        var result = ParseLiveQueryManager.SubscribeToQuery(queryID);
                        if(result.createSubscription)
                        {
                            var query = new Parse.Query(this);
                            query.equalTo("hospital", hospital);                        
                            query.ascending("bedType");

                            ParseLiveQueryManager.CreateSubscription(queryID, query)
                        }
                        return result.retObject;
                    },

                    Unsubscribe : function(subToken) {
                        ParseLiveQueryManager.RemoveSubscription(subToken);
                    }
                });

            ParseHelperService.patchBindings(model, ['occupancy', 'wardType', 'bedType', 'red', 'yellow', 'green', 'hospital']);
            return model;
        });
})();

