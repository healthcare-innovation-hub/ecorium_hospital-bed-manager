(function () {
    'use strict';

    angular.module('app.emergency')
        .controller('SidebarCtrl', ['$rootScope','$scope', '$mdToast', '$location' ,'Emergency', 'AuthService', SidebarCtrl])
        .controller('ToastCtrl', ['$scope', '$mdToast', ToastCtrl])

    function SidebarCtrl($rootScope, $scope, $mdToast, $location, Emergency, AuthService) {

        $scope.GetLength = function(obj) {
            return Object.keys(obj).length;
        }

        $scope.emergencies = {};
        var sub = Emergency.SubActiveEmergencies();
        $scope.emergencies = sub.initstate;
        sub.promise.then(null, null, function(update) {
            if(update.action=="create")
            {
                $mdToast.show({
                    controller: 'ToastCtrl',
                    templateUrl: 'toast-template.html',
                    hideDelay: 0,
                    position: "top right"
                }).then(function(val){
                    if(val=="show")
                        $location.path('/app/emergency-map/' + update.objectID);
                });

            }
            $scope.emergencies = update.state;
        })

        $scope.finishedEmergencies = {};
        var finish = Emergency.SubFinishedEmergencies();
        $scope.finishedEmergencies = finish.initstate;
        finish.promise.then(null, null, function(update) {
            $scope.finishedEmergencies = update.state;
        })

        $scope.$on("$destroy", function handler() {
            Emergency.Unsubscribe(sub.token);
        });
    }

    function ToastCtrl($scope, $mdToast) {
        $scope.closeToast = function(val) {
            $mdToast.hide(val);
        };
    }
})();
