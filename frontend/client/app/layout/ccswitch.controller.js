(function () {
    'use strict';

    angular.module('app.emergency')
        .controller('CCSwitchCtrl', function SidebarCtrl(ControlCenter, $scope, $cookies, $window) {
            let currentCCId = (Parse.User.current().get("controlCenterRelation")) ? Parse.User.current().get("controlCenterRelation").id : "";
            let controlCenters
            $scope.select = { selectedText: null, selectedControlCenter: null, controlCenterList: [], color: 'black' };

            ControlCenter.list().then(function (controlCenterList) {
                $scope.select.controlCenterList = controlCenterList;

                for (let controlCenter of controlCenterList) {
                    if (controlCenter.id == currentCCId)
                        $scope.select.selectedControlCenter = controlCenter;
                }
            })

            $scope.querySearch = function (query) {
                return query ? $scope.select.controlCenterList.filter(function (controlCenter) {
                    var lowercaseQuery = angular.lowercase(query);
                    var lowercaseName = angular.lowercase(controlCenter.name);
                    return lowercaseName.indexOf(lowercaseQuery) >= 0;
                }) : $scope.select.controlCenterList;
            }

            $scope.onCCChange = function (controlCenter) {
                $scope.select.color = (controlCenter == null) ? 'red' : 'black';
                let currentCCId = (Parse.User.current().get("controlCenterRelation")) ? Parse.User.current().get("controlCenterRelation").id : "";
                if (controlCenter != null && controlCenter.id != currentCCId) {
                    $cookies.putObject("su.CCSwitchId", controlCenter.id);
                    $window.location.reload();
                }
            }
        })
})();
