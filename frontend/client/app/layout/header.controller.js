(function () {
    angular.module('app.layout')
        .controller('HeaderCtrl', ['$scope', '$state','AuthService', HeaderCtrl])

    function HeaderCtrl ($scope, $state, AuthService, Hospital) {

        // depend on google mateial icons
        $scope.logoutUser = function() {
            AuthService.logout().then(function(){
                $state.go('signin');
            });
        }

        $scope.getUsername = function() {
            if(Parse.User.current())
                return Parse.User.current().get('username');
            else
                return null;
        }

        // Get hospital for current user
        $scope.hospital = "Belegungsmanager";
        var currentUser = Parse.User.current();
        var query = new Parse.Query('Hospital');
        query.equalTo('users', currentUser);
        query.first().then(function (hospital){
            
            $scope.hospital = hospital.get('name');

        }, function(error){
            console.log(error);
        });
    }
})();
