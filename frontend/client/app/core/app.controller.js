(function () {
    'use strict';

    angular.module('app')
        .controller('AppCtrl', ['$scope', '$rootScope', '$document', 'appConfig', '$state', '$mdSidenav', '$mdComponentRegistry', '$urlRouter', 'AuthService', AppCtrl]) // overall control
        .controller('SidenavRightCtrl', ['$scope', '$mdSidenav', SidenavRightCtrl])

    function AppCtrl($scope, $rootScope, $document, appConfig, $state, $mdSidenav, $mdComponentRegistry, $urlRouter, AuthService) {
        $scope.pageTransitionOpts = appConfig.pageTransitionOpts;
        $scope.app = appConfig.app;
        $scope.color = appConfig.color;

        // Checks if the given state is the current state
        $scope.is = function (name) {
            return $state.is(name);
        };

        // Checks if the given state/child states are present
        $scope.includes = function (name) {
            return $state.includes(name);
        };

        $rootScope.templateBase = 'app/component/';
        $rootScope.color = appConfig.color;
        $rootScope.AuthService = AuthService;

        $rootScope.gotoHome = function() {
            if(AuthService.isInitialized())
                $state.go( AuthService.getMainRole().get("startState") );
            else
                $state.go('signin');
        }

        $rootScope.$on('$stateChangeStart', function (event, next, nextParams, fromState) {
            if (!AuthService.isInitialized()) {
                AuthService.initialize().then(function () {
                    $urlRouter.sync()
                })
                event.preventDefault();
            } else if (!AuthService.isAuthenticated()) {
                if (next.name !== 'signin') {
                    event.preventDefault();
                    $state.go('signin');
                }
            } else if ('params' in next && 'privilege' in next.params && !AuthService.hasPrivilege(next.params.privilege)) {
                event.preventDefault();
                $state.go('403');
            }
        })


        //
        $rootScope.$on("$stateChangeSuccess", function (event, currentRoute, previousRoute) {
            $document.duScrollTo(0, 0);
        });


        // for mdSideNav right
        $scope.toggleRight = buildToggler('right');
        $scope.isOpenRight = function () {
            if (!$mdComponentRegistry.get('right')) return;

            return $mdSidenav('right').isOpen();
        };

        function buildToggler(navID) {
            return function () {
                // Component lookup should always be available since we are not using `ng-if`
                $mdSidenav(navID).toggle();
            }
        }
    }


    function SidenavRightCtrl($scope, $mdSidenav) {
        $scope.close = function () {
            // Component lookup should always be available since we are not using `ng-if`
            $mdSidenav('right').close()
        };
    }

})();
