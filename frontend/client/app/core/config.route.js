
angular.module('app')
    .config(['$stateProvider', '$urlRouterProvider', '$ocLazyLoadProvider', 'AuthServiceProvider',
        function ($stateProvider, $urlRouterProvider, $ocLazyLoadProvider, AuthServiceProvider) {

            $urlRouterProvider.when('', function ($injector, $location) {
                var $state = $injector.get("$state");
                $state.go("signin");
            });
            $urlRouterProvider.otherwise('404');

            const templateBase = 'app/component/';
            const templateBaseDemoComponent = 'app/demo-component/';

            $stateProvider
                // Overall
                .state('app', {
                    url: '/app',
                    templateUrl: "app/app.html",
                    resolve: {
                        deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                            return $ocLazyLoad.load([
                                'googlemap',
                            ]);
                        }]
                    }
                })

                .state('app2', {
                    url: '/app2',
                    templateUrl: "app/app2.html" // No app-sidebar, the rest is the same with app.html
                })


                //PS Implementations
                .state('app.map', {
                    url: '/map',
                    templateUrl: templateBase + 'map/map.html',
                    params: {
                        privilege: 'route.map'
                    }
                })

                .state('app.contingents', {
                    url: '/contingents',
                    templateUrl: templateBase + 'contingents/contingents.html',
                    params: {
                        privilege: 'route.contingents'
                    }
                })

                .state('app.hospital-list', {
                    url: '/hospital-list',
                    templateUrl: templateBase + 'hospital/_list/hospital-list.html',
                    params: {
                        privilege: 'route.hospitalList'
                    }
                })

                .state('app.hospital', {
                    url: '/hospital/{id}',
                    templateUrl: templateBase + 'hospital/_single/hospital.html',
                    params: {
                        privilege: 'route.hospital'
                    }
                })

                .state('app.user-list', {
                    url: '/user-list',
                    templateUrl: templateBase + 'user/_list/user-list.html',
                    params: {
                        privilege: 'route.userList'
                    }
                })

                .state('app.logfile', {
                    url: '/logfile',
                    templateUrl: templateBase + 'logfile/_list/logfile-list.html',
                    params: {
                        privilege: 'route.logfile'
                    }
                })

                //Signin
                .state('signin', {
                    url: '/signin',
                    templateUrl: templateBase + 'authentication/signin.html',
                })

                // Form
                .state('app.form-elements', {
                    url: '/form/element/elements',
                    templateUrl: templateBaseDemoComponent + 'form/element/elements.html'
                })
                .state('app.form-autocomplete', {
                    url: '/form/element/autocomplete',
                    templateUrl: templateBaseDemoComponent + 'form/element/autocomplete.html'
                })
                .state('app.form-checkbox', {
                    url: '/form/element/checkbox',
                    templateUrl: templateBaseDemoComponent + 'form/element/checkbox.html'
                })
                .state('app.form-chips', {
                    url: '/form/element/chips',
                    templateUrl: templateBaseDemoComponent + 'form/element/chips.html'
                })
                .state('app.form-datepicker', {
                    url: '/form/element/datepicker',
                    templateUrl: templateBaseDemoComponent + 'form/element/datepicker.html'
                })
                .state('app.form-input', {
                    url: '/form/element/input',
                    templateUrl: templateBaseDemoComponent + 'form/element/input.html'
                })
                .state('app.form-radio-button', {
                    url: '/form/element/radio-button',
                    templateUrl: templateBaseDemoComponent + 'form/element/radio-button.html'
                })
                .state('app.form-select', {
                    url: '/form/element/select',
                    templateUrl: templateBaseDemoComponent + 'form/element/select.html'
                })
                .state('app.form-slider', {
                    url: '/form/element/slider',
                    templateUrl: templateBaseDemoComponent + 'form/element/slider.html'
                })
                .state('app.form-switch', {
                    url: '/form/element/switch',
                    templateUrl: templateBaseDemoComponent + 'form/element/switch.html'
                })

                .state('app.form-validation', {
                    url: '/form/validation',
                    templateUrl: templateBaseDemoComponent + 'form/validation.html'
                })
                .state('app.form-wizard', {
                    url: '/form/wizard',
                    templateUrl: templateBaseDemoComponent + 'form/wizard.html',
                    resolve: {
                        deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                            return $ocLazyLoad.load([
                                'angular-wizard'
                            ]);
                        }]
                    }
                })
                .state('app.form-editor', {
                    url: '/form/editor',
                    templateUrl: templateBaseDemoComponent + 'form/editor.html',
                    resolve: {
                        deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                            return $ocLazyLoad.load([
                                'fontawesome',
                                'textAngular'
                            ]);
                        }]
                    }
                })
                .state('app.form-layouts', {
                    url: '/form/layouts',
                    templateUrl: templateBaseDemoComponent + 'form/layouts.html'
                })


                // charts
                .state('app.chart-line', {
                    url: '/chart/line',
                    templateUrl: templateBaseDemoComponent + 'chart/line.html'
                })
                .state('app.chart-bar', {
                    url: '/chart/bar',
                    templateUrl: templateBaseDemoComponent + 'chart/bar.html'
                })
                .state('app.chart-pie', {
                    url: '/chart/pie',
                    templateUrl: templateBaseDemoComponent + 'chart/pie.html'
                })
                .state('app.chart-scatter', {
                    url: '/chart/scatter',
                    templateUrl: templateBaseDemoComponent + 'chart/scatter.html'
                })
                .state('app.chart-radar', {
                    url: '/chart/radar',
                    templateUrl: templateBaseDemoComponent + 'chart/radar.html'
                })
                .state('app.chart-funnel', {
                    url: '/chart/funnel',
                    templateUrl: templateBaseDemoComponent + 'chart/funnel.html'
                })
                .state('app.chart-gauge', {
                    url: '/chart/gauge',
                    templateUrl: templateBaseDemoComponent + 'chart/gauge.html'
                })
                .state('app.chart-more', {
                    url: '/chart/more',
                    templateUrl: templateBaseDemoComponent + 'chart/more.html'
                })


                // Page Layout
                .state('app.page-layout-blank-full', {
                    url: '/pglayout/blank-full',
                    templateUrl: templateBaseDemoComponent + 'page-layout/blank-full.html'
                })
                .state('app.page-layout-blank-centered', {
                    url: '/pglayout/blank-centered',
                    templateUrl: templateBaseDemoComponent + 'page-layout/blank-centered.html'
                })
                .state('app.page-layout-blank-tabs', {
                    url: '/pglayout/blank-with-tabs',
                    templateUrl: templateBaseDemoComponent + 'page-layout/blank-with-tabs.html'
                })

                .state('blank', {
                    url: '/blank',
                    templateUrl: templateBaseDemoComponent + 'page-layout/blank.html'
                })

                .state('app2.page-layout-blank-full', {
                    url: '/pglayout/blank-full',
                    templateUrl: templateBaseDemoComponent + 'page-layout/blank-full.html'
                })
                .state('app2.page-layout-blank-centered', {
                    url: '/pglayout/blank-centered',
                    templateUrl: templateBaseDemoComponent + 'page-layout/blank-centered.html'
                })

                // Extra
                .state('403', {
                    url: '/403',
                    templateUrl: templateBaseDemoComponent + "page-extra/403.html"
                })
                .state('404', {
                    url: '/404',
                    templateUrl: templateBaseDemoComponent + "page-extra/404.html"
                })
                .state('500', {
                    url: '/500',
                    templateUrl: templateBaseDemoComponent + "page-extra/500.html"
                })
                .state('signup', {
                    url: '/signup',
                    templateUrl: templateBaseDemoComponent + 'page-extra/signup.html'
                })
                .state('forgot-password', {
                    url: '/forgot-password',
                    templateUrl: templateBaseDemoComponent + 'page-extra/forgot-password.html'
                })
                .state('confirm-email', {
                    url: '/confirm-email',
                    templateUrl: templateBaseDemoComponent + 'page-extra/confirm-email.html'
                })
                .state('lock-screen', {
                    url: '/lock-screen',
                    templateUrl: templateBaseDemoComponent + 'page-extra/lock-screen.html'
                })
                .state('maintenance', {
                    url: '/maintenance',
                    templateUrl: templateBaseDemoComponent + 'page-extra/maintenance.html'
                })

                //ALL FOLLOWING ROUTES ARE FOR DEMO DATA (DELTE)
                // UI
                .state('app.ui-buttons', {
                    url: '/ui/buttons',
                    templateUrl: templateBaseDemoComponent + 'ui/buttons.html',
                    resolve: {
                        deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                            return $ocLazyLoad.load([
                                'fontawesome' // for social icons
                            ]);
                        }]
                    }
                })
                .state('app.ui-typography', {
                    url: '/ui/typography',
                    templateUrl: templateBaseDemoComponent + 'ui/typography.html'
                })
                .state('app.ui-icons', {
                    url: '/ui/icons',
                    templateUrl: templateBaseDemoComponent + 'ui/icons.html',
                    resolve: {
                        deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                            return $ocLazyLoad.load([
                                'fontawesome',
                                "weather-icons"
                            ]);
                        }]
                    }
                })
                .state('app.ui-lists', {
                    url: '/ui/lists',
                    templateUrl: templateBaseDemoComponent + 'ui/lists.html'
                })
                .state('app.ui-components', {
                    url: '/ui/components',
                    templateUrl: templateBaseDemoComponent + 'ui/components.html'
                })
                .state('app.ui-boxes', {
                    url: '/ui/boxes',
                    templateUrl: templateBaseDemoComponent + 'ui/boxes.html'
                })
                .state('app.ui-icon-boxes', {
                    url: '/ui/icon-boxes',
                    templateUrl: templateBaseDemoComponent + 'ui/icon-boxes.html'
                })
                .state('app.ui-hover', {
                    url: '/ui/hover',
                    templateUrl: templateBaseDemoComponent + 'ui/hover.html'
                })
                .state('app.ui-cards', {
                    url: '/ui/cards',
                    templateUrl: templateBaseDemoComponent + 'ui/cards.html'
                })
                .state('app.ui-sash', {
                    url: '/ui/sash',
                    templateUrl: templateBaseDemoComponent + 'ui/sash.html'
                })
                .state('app.ui-testimonials', {
                    url: '/ui/testimonials',
                    templateUrl: templateBaseDemoComponent + 'ui/testimonials.html'
                })
                .state('app.ui-pricing-tables', {
                    url: '/ui/pricing-tables',
                    templateUrl: templateBaseDemoComponent + 'ui/pricing-tables.html'
                })
                .state('app.ui-call-to-actions', {
                    url: '/ui/call-to-actions',
                    templateUrl: templateBaseDemoComponent + 'ui/call-to-actions.html'
                })
                .state('app.ui-feature-callouts', {
                    url: '/ui/feature-callouts',
                    templateUrl: templateBaseDemoComponent + 'ui/feature-callouts.html'
                })
                .state('app.ui-timeline', {
                    url: '/ui/timeline',
                    templateUrl: templateBaseDemoComponent + 'ui/timeline.html',
                    resolve: {
                        deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                            return $ocLazyLoad.load([
                                'fontawesome' // for icons
                            ]);
                        }]
                    }
                })
                .state('app.ui-maps', {
                    url: '/ui/maps',
                    templateUrl: templateBaseDemoComponent + 'ui/maps.html',
                    resolve: {
                        deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                            return $ocLazyLoad.load([
                                'googlemap',
                            ]);
                        }]
                    }

                })
                .state('app.ui-grids', {
                    url: '/ui/grids',
                    templateUrl: templateBaseDemoComponent + 'ui/grids.html'
                })
                .state('app.ui-color', {
                    url: '/ui/color',
                    templateUrl: templateBaseDemoComponent + 'ui/color.html'
                })

                // Tables
                .state('app.table-static', {
                    url: '/table/static',
                    templateUrl: templateBaseDemoComponent + 'table/static.html'
                })
                .state('app.table-responsive', {
                    url: '/table/responsive',
                    templateUrl: templateBaseDemoComponent + 'table/responsive.html'
                })
                .state('app.table-data', {
                    url: '/table/data',
                    templateUrl: templateBaseDemoComponent + 'table/data.html'
                })

                // Page
                .state('app.page-profile', {
                    url: '/page/profile',
                    templateUrl: templateBaseDemoComponent + 'page/profile.html'
                })
                .state('app.page-faqs', {
                    url: '/page/faqs',
                    templateUrl: templateBaseDemoComponent + 'page/faqs.html'
                })
                .state('app.page-blog', {
                    url: '/page/blog',
                    templateUrl: templateBaseDemoComponent + 'page/blog.html'
                })
                .state('app.page-gallery', {
                    url: '/page/gallery',
                    templateUrl: templateBaseDemoComponent + 'page/gallery.html'
                })
                .state('app.page-portfolio', {
                    url: '/page/portfolio',
                    templateUrl: templateBaseDemoComponent + 'page/portfolio.html'
                })
                .state('app.page-about', {
                    url: '/page/about',
                    templateUrl: templateBaseDemoComponent + 'page/about.html'
                })
                .state('app.page-team', {
                    url: '/page/team',
                    templateUrl: templateBaseDemoComponent + 'page/team.html'
                })
                .state('app.page-services', {
                    url: '/page/services',
                    templateUrl: templateBaseDemoComponent + 'page/services.html'
                })
                .state('app.page-contact', {
                    url: '/page/contact',
                    templateUrl: templateBaseDemoComponent + 'page/contact.html'
                })
                .state('app.page-careers', {
                    url: '/page/careers',
                    templateUrl: templateBaseDemoComponent + 'page/careers.html'
                })
                .state('app.page-privacy', {
                    url: '/page/privacy',
                    templateUrl: templateBaseDemoComponent + 'page/privacy.html'
                })
                .state('app.page-terms', {
                    url: '/page/terms',
                    templateUrl: templateBaseDemoComponent + 'page/terms.html'
                })

                // eCommerce
                .state('app.ecommerce-products', {
                    url: '/ecommerce/products',
                    templateUrl: templateBaseDemoComponent + 'ecommerce/products.html'
                })
                .state('app.ecommerce-horizontal-products', {
                    url: '/ecommerce/horizontal-products',
                    templateUrl: templateBaseDemoComponent + 'ecommerce/horizontal-products.html'
                })
                .state('app.ecommerce-invoice', {
                    url: '/ecommerce/invoice',
                    templateUrl: templateBaseDemoComponent + 'ecommerce/invoice.html'
                })

                // App
                .state('app.app-calendar', {
                    url: '/app/calendar',
                    templateUrl: templateBaseDemoComponent + 'app/calendar/calendar.html',
                    resolve: {
                        deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                            return $ocLazyLoad.load([
                                'fullcalendar'
                            ]);
                        }]
                    }
                })
                .state('app.app-task', {
                    url: '/app/task',
                    templateUrl: templateBaseDemoComponent + 'app/task/task.html'
                })


                // Email
                .state('app.email', {
                    url: '/email',
                    templateUrl: templateBaseDemoComponent + "email/email.html",
                    controller: 'MailController',
                    resolve: {
                        deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                            return $ocLazyLoad.load([
                                'email'
                            ]);
                        }]
                    }
                })
                .state('app.email.inbox', {
                    url: '/inbox',
                    templateUrl: templateBaseDemoComponent + 'email/inbox.html'
                })
                .state('app.email.detail', {
                    url: '/{id:[0-9]{1,4}}',
                    templateUrl: templateBaseDemoComponent + 'email/detail.html'
                })
                .state('app.email.compose', {
                    url: '/compose',
                    templateUrl: templateBaseDemoComponent + 'email/compose.html'
                })

                ;
        }
    ]);
