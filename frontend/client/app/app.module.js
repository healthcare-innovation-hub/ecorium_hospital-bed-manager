(function () {
    'use strict';

    angular.module('app', [
        // Angular modules
         'ui.router'
        ,'ngAnimate'
        ,'ngAria'
        ,'ngCookies'
        //,'ngMessages'

        // 3rd Party Modules
        ,'oc.lazyLoad'
        ,'ngMaterial'
        ,'lfNgMdFileInput'
        ,'duScroll'
        ,'ngOrderObjectBy'
        ,'ngSanitize'
        ,'angular-toArrayFilter'
        ,'angularMoment'

        ,'app.layout'
        ,'app.i18n'

        ,'app.chart'
        ,'app.emergency'
        ,'app.map'
        ,'app.contingent'
        ,'app.user'
        ,'app.logfile'
        ,'app.hospital'
        ,'app.authentication'
        ,'app.form'
        ,'app.form.validation'

    ])
        .run(function(amMoment, $rootScope) {
            amMoment.changeLocale('de');
            Parse.initialize("FGIR465Fjgkn5459fgkjmqpru0vbcm", "UGNSNG89456zNSIUDBUfdiugbuzdgf487z");
            Parse.serverURL = "https://krankenhaus-belegung.herokuapp.com/parse";
        });

})();







