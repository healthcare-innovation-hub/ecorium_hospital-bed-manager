(function () {
    'use strict';

    angular.module('app.hospital')
        .controller('ContingentsCtrl', function ($scope, $timeout, $rootScope, $mdDialog, $state, Hospital, Occupancy, Logfile, User) {

            $scope.loadData = function () {

                $rootScope.$broadcast('preloader:active');

                // Get hospital for current user
                var currentUser = Parse.User.current();
                var query = new Parse.Query(Hospital);
                query.equalTo('users', currentUser);
                query.first().then(function (hospital){
                    
                    $scope.hospital = hospital;

                    // Get occupancy settings for hospital (grouped by ward type)
                    getOccupancySettings(hospital);

                }, function(error){
                    console.log(error);
                });

                // Populate dropdown
                User.getRoleForUser(currentUser).then(function(role){

                    $scope.userRole = role.get('name');
                    if (role.get('name') === 'Administrators')
                    {
                        // Get all hospitals
                        Hospital.list().then(function(hospitals) {
                            
                            $scope.hospitals = hospitals;

                        }, function(error) {
                            console.log(error);
                        });
                    }
                });
            };
            $scope.loadData();

            $scope.updateOccupancy = function(occupancy){

                // Save updates
                Occupancy.updateOccupancy(occupancy);

                // Write logfile
                Logfile.logChange(occupancy);
            };

            $scope.changeHospital = function(hospital){

                $scope.hospital = hospital;
                getOccupancySettings(hospital);
            };

            function getOccupancySettings(hospital){

                $scope.hospital.wards = [];
                Occupancy.getByHospital(hospital, 'INTERNAL_MEDICIN').then(function(occupancies){

                    var newWard = {
                        wardType:   'INTERNAL_MEDICIN',
                        occupancies: occupancies 
                    };

                    $scope.hospital.wards.push(newWard);
                });
                Occupancy.getByHospital(hospital, 'SURGERY').then(function(occupancies){
                    
                    var newWard = {
                        wardType:   'SURGERY',
                        occupancies: occupancies 
                    };

                    $scope.hospital.wards.push(newWard);
                });
            }
        })
})();