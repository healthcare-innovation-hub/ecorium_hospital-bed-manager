(function () {
    'use strict';

    angular.module('app.logfile')
        .controller('LogfileListCtrl', function ($scope, $timeout, $rootScope, Logfile) {

        $scope.loadData = function () {

            $rootScope.$broadcast('preloader:active');

            Logfile.list().then(function(logfile) {
                
                $scope.logfile = logfile;
                $rootScope.$broadcast('preloader:hide');

            }, function(aError) {
                // Something went wrong, handle the error
            });
        };
        $scope.loadData();
    })
})();
