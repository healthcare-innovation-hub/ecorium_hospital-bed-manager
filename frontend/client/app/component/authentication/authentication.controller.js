(function () {
    'use strict';

    angular.module('app.authentication')
        .controller('AuthenticationCtrl', ['$rootScope', '$scope', '$mdToast','$state', 'AuthService', AuthenticationCtrl])
        .controller('ToastCtrl', ['$scope','$mdToast', ToastCtrl])

    function AuthenticationCtrl($rootScope,$scope, $mdToast, $state, AuthService) {
        var original;

        $scope.user = {
            email: '',
            password: ''
        }

        original = angular.copy($scope.user);
        // https://github.com/angular/material/issues/1903
        $scope.revert = function() {
            $scope.user = angular.copy(original);
            $scope.material_login_form.$setPristine();
            $scope.material_login_form.$setUntouched();
            return;
        };
        $scope.canRevert = function() {
            return !angular.equals($scope.user, original) || !$scope.material_login_form.$pristine;
        };
        $scope.canSubmit = function() {
            return $scope.material_login_form.$valid && !angular.equals($scope.user, original);
        };
        $scope.submitForm = function() {
            $scope.showInfoOnSubmit = true;
            AuthService.login($scope.user.email,$scope.user.password).then(function(user){
                if(AuthService.isAuthenticated()) {
                    $rootScope.gotoHome();
                } else {
                    AuthService.logout().then(function(){
                        $state.go('signin');
                    })
                }
            }, function(error){
                $mdToast.show({
                    template: '<md-toast class="md-toast-danger">{{ toast.content }}</md-toast>',
                    controller: [function () {
                        this.content = error;
                    }],
                    controllerAs: 'toast',
                    position: 'bottom right',
                    hideDelay: 4000
                });

                AuthService.logout();

            });

        };
    }

    function ToastCtrl($scope, $mdToast) {
        $scope.closeToast = function(val) {
            $mdToast.hide(val);
        };
    }

})();
