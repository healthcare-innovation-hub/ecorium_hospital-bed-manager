(function () {
    'use strict';

    angular.module('app.user')
        .controller('UserListCtrl', function ($scope, $timeout, $rootScope, Hospital, User) {

        $scope.currentUserId = Parse.User.current().id;
        
        $scope.loadData = function () {

            $rootScope.$broadcast('preloader:active');

            // Get all roles for usage as select options
            var query = new Parse.Query(Parse.Role);
            query.find().then(function(roles){
                $scope.roles = roles;
            });

            // Get all hospitals for usage as select options
            Hospital.list().then(function(hospitals){
                $scope.hospitals = hospitals;
            });

            User.listUsers().then(function(users) {
                $scope.users = users;
                $rootScope.$broadcast('preloader:hide');
            }, function(aError) {
                // Something went wrong, handle the error
            });
        };
        $scope.loadData();

        $scope.updateRole = function(user){
            
            var oldRole = null;
            var newRole = user.role;

            // Get previous role
            var query = new Parse.Query(Parse.Role);
            query.equalTo('users', user);
            query.first().then(function (role){
                
                oldRole = role;
                if (oldRole) // In case user had a role before
                {
                    // Delete user from previous role
                    oldRole.getUsers().remove(user);
                    oldRole.save();
                }
                else // In case user did not have a role before
                {
                    // Create user object for this user
                    newRole     = new Parse.Role;
                    newRole.id  = user.role.id;
                    user.role   = newRole;
                }    

                // Save user to new role
                newRole.getUsers().add(user);
                newRole.save();

                // Delete user from hospital if user has become a regular "User" now
                if(user.role.getName() === 'User' && user.hospital)
                {
                    user.hospital.id = null;
                    $scope.updateHospital(user);
                }
            });
        };

        $scope.updateHospital = function(user){

            var oldHospital = null;
            var newHospital = user.hospital;

            // Get previous hospital
            var query = new Parse.Query(Hospital);
            query.equalTo('users', user);
            query.first().then(function (hospital){

                oldHospital = hospital;
                if (oldHospital) // In case user was already assigned to a hospital
                {
                    // Remove user from previous hospital
                    oldHospital.get('users').remove(user);
                    oldHospital.save();
                }
                else // In case user wasn't assigned to any hospital yet
                {
                    // Create hospital object for this user
                    newHospital = new Hospital();
                    newHospital.id = user.hospital.id;
                }

                // Assign user to new hospital
                if (newHospital.id !== null)
                {
                    newHospital.get('users').add(user);
                    newHospital.save();
                }

                // Update header
                if ($scope.currentUserId === user.id)
                {
                    var headline = document.getElementById('headline');
                    headline.innerHTML = newHospital.get('name');
                }
            });
        };
    })
})();
