(function () {
    'use strict';

    angular.module('app.user').
    factory('User',  function($q, AuthService, ParseHelperService, Hospital) {

        var User = Parse.Object.extend("User", {
            // Instance methods

        }, {
            // Rest-API Methods
            listUsers : function() {
                var defer = $q.defer();
                var query = CreateQuery();
                query.include('hospital');
                query.find({
                    success : function(userList) {

                        var promises = [];
                        userList.forEach(function (user){

                            // Get role for this user
                            var query = new Parse.Query(Parse.Role);
                            query.equalTo('users', user);
                            
                            var promise = query.first().then(function (role){
                                user.role = role;
                            });
                            promises.push(promise);

                            // Get hospital for this user
                            var query = new Parse.Query(Hospital);
                            query.equalTo('users', user);
                            
                            var promise = query.first().then(function (hospital){
                                user.hospital = hospital;
                            });
                            promises.push(promise);
                        });

                        // Wait until foreach loop has finished
                        Parse.Promise.when(promises).then(function(){
                            defer.resolve(userList)
                        });
                    },

                    error : function(aError) {
                        defer.reject(aError);
                    }
                });

                return defer.promise;
            },

            getRoleForUser : function(user){

                var defer = $q.defer();
                
                var query = new Parse.Query(Parse.Role);
                query.equalTo('users', user);
                
                query.first().then(function (role){
                    defer.resolve(role);
                });

                return defer.promise;
            }
        });

        function CreateQuery(){
            var query = new Parse.Query(User);
            return query;
        }

        ParseHelperService.patchBindings(User, ['username', 'role']);
        return User;
    });
})();
