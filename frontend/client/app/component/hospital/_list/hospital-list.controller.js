(function () {
    'use strict';

    angular.module('app.hospital')
        .controller('HospitalListCtrl', function ($scope, $timeout, $rootScope, $mdDialog, Hospital, Occupancy) {

        $scope.loadData = function () {

            $rootScope.$broadcast('preloader:active');

            Hospital.list().then(function(hospitals) {
                $scope.hospitals = hospitals;
                $rootScope.$broadcast('preloader:hide');

            }, function(error) {
                console.log(error);
            });
        };
        $scope.loadData();

        $scope.createHospitalPopup = function(event){

            $mdDialog.show({
                controller: DialogController,
                templateUrl: $scope.templateBase + 'hospital/_list/popover-create-hospital.html',
                parent: angular.element(document.body),
                targetEvent: event,
                clickOutsideToClose: true,
                fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
            });
        };

        $scope.deleteHospital = function(event, hospital){

            // Appending dialog to document.body
            var confirm = $mdDialog.confirm()
                  .title('Krankenhaus löschen?')
                  .textContent('Soll ' + hospital.name + ' wirklich gelöscht werden?')
                  .ariaLabel('Krankenhaus löschen')
                  .targetEvent(event)
                  .ok('Löschen!')
                  .cancel('Abbrechen');

            $mdDialog.show(confirm).then(function() {

                // Delete all occupancies for this hospital
                Occupancy.getByHospital(hospital).then(function(occupancies){

                    Parse.Object.destroyAll(occupancies, {

                        success: function(){
                            hospital.destroy({
                                success: function(response){
                                            
                                    $rootScope.$broadcast('preloader:active');

                                    Hospital.list().then(function(hospitals) {
                                        $scope.hospitals = hospitals;
                                        $rootScope.$broadcast('preloader:hide');

                                    }, function(error) {
                                        console.log(error);
                                    });

                                }, error: function(response, error){
                                    console.log(error);
                                }
                            });
                        },
                        error: function(error){
                            console.log(error);
                        },
                    });
                });
              

            }, function() {
                console.log("Error showing dialog.")
            });
        }

        function DialogController($scope, $mdDialog){

            // Create new hospital to be registered
            var newHospital = new Hospital();
            $scope.newHospital = newHospital;

            // Setup geocoding
            var geocoder = new google.maps.Geocoder();
            $scope.findAddress = function(){

                if (geocoder !== undefined) {
                    geocoder.geocode(
                        { address: $scope.newHospital.address },
                        function (results, status) {
                            $scope.places = [];
                            switch (status) 
                            {
                                case google.maps.GeocoderStatus.OK:
                                    $scope.places = results;
                                    if (results.length < 2) 
                                    {
                                        $scope.registration.address.$setValidity("address", true);

                                        // Get formatted address
                                        $scope.newHospital.address = results[0].formatted_address;

                                        // Get lng & lat
                                        $scope.newHospital.location     = new Parse.GeoPoint;
                                        $scope.newHospital.location.latitude = results[0].geometry.location.lat();
                                        $scope.newHospital.location.longitude = results[0].geometry.location.lng();
                                    } 
                                    else 
                                    {
                                        $scope.registration.address.$setValidity("address", false);
                                    }
                                    break;
                                case google.maps.GeocoderStatus.ZERO_RESULTS:
                                    console.log('zero results');
                                    $scope.registration.address.$setValidity("address", false);
                                    break;
                                case google.maps.GeocoderStatus.REQUEST_DENIED:
                                    $scope.registration.address.$setValidity("address", false);
                                    console.log('request denied');
                                    break;
                                case google.maps.GeocoderStatus.INVALID_REQUEST:
                                    $scope.registration.address.$setValidity("address", false);
                                    console.log('invalid request');
                                    break;
                            }
                            $scope.$apply();
                        }
                    );
                }
            }

            $scope.hide = function() {
                $mdDialog.hide();
            };

            $scope.cancel = function() {
                $mdDialog.hide();
            };

            $scope.register = function() {

                $scope.newHospital.save().then(function(hospital){

                    addNewHospitalToList(hospital);
                    $mdDialog.hide();

                    // Set default occupancies for new hospital
                    Occupancy.createDefaults(hospital).then(function(){

                    }, function(error){
                        console.log(error);
                    });

                }, function(error){
                    console.log(error);
                });
            };
        }

        function addNewHospitalToList(hospital)
        {
            $scope.hospitals.push(hospital);
        }
    })
})();