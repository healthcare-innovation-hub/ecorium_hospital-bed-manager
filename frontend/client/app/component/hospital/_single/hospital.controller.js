(function () {
    'use strict';

    angular.module('app.hospital')
        .controller('HospitalCtrl', function ($scope, $timeout, $rootScope, $mdDialog, $state, Hospital, Occupancy, Logfile) {

            $scope.loadData = function () {

                var hospitalId = $state.params.id;

                $rootScope.$broadcast('preloader:active');

                Hospital.getById(hospitalId).then(function(hospital) {
                    $scope.hospital = hospital;

                    // Get occupancy settings for hospital (grouped by ward type)
                    $scope.hospital.wards = [];
                    Occupancy.getByHospital(hospital, 'INTERNAL_MEDICIN').then(function(occupancies){

                        var newWard = {
                            wardType:   'INTERNAL_MEDICIN',
                            occupancies: occupancies 
                        };

                        $scope.hospital.wards.push(newWard);
                    });
                    Occupancy.getByHospital(hospital, 'SURGERY').then(function(occupancies){
                        
                        var newWard = {
                            wardType:   'SURGERY',
                            occupancies: occupancies 
                        };

                        $scope.hospital.wards.push(newWard);
                    });

                }, function(error) {
                    console.log(error);
                });
            };
            $scope.loadData();

            // Setup geocoding
            var geocoder = new google.maps.Geocoder();
            $scope.findAddress = function(){

                if (geocoder !== undefined) {
                    geocoder.geocode(
                        { address: $scope.hospital.address },
                        function (results, status) {
                            $scope.places = [];
                            switch (status) 
                            {
                                case google.maps.GeocoderStatus.OK:
                                    $scope.places = results;
                                    if (results.length < 2) 
                                    {
                                        $scope.editHospital.address.$setValidity("address", true);

                                        // Get formatted address
                                        $scope.hospital.address = results[0].formatted_address;

                                        // Get lng & lat
                                        $scope.hospital.location     = {};
                                        $scope.hospital.location.lat = results[0].geometry.location.lat();
                                        $scope.hospital.location.lng = results[0].geometry.location.lng();

                                        $scope.hospital.save();
                                    } 
                                    else 
                                    {
                                        $scope.editHospital.address.$setValidity("address", false);
                                    }
                                    break;
                                case google.maps.GeocoderStatus.ZERO_RESULTS:
                                    console.log('zero results');
                                    $scope.editHospital.address.$setValidity("address", false);
                                    break;
                                case google.maps.GeocoderStatus.REQUEST_DENIED:
                                    $scope.editHospital.address.$setValidity("address", false);
                                    console.log('request denied');
                                    break;
                                case google.maps.GeocoderStatus.INVALID_REQUEST:
                                    $scope.editHospital.address.$setValidity("address", false);
                                    console.log('invalid request');
                                    break;
                            }
                            $scope.$apply();
                        }
                    );
                }
            };

            $scope.updateName = function(){

                if ($scope.hospital.name === undefined)
                {
                    return false;
                }
                else
                {
                    $scope.hospital.save();
                }
            };

            $scope.updateThreshold = function(occupancy, color){
               
                switch(color)
                {
                    case 'RED': 
                    {
                         // Check if input is empty or contains letters or is smaller than allowed
                        if((occupancy.red.max.match && !occupancy.red.max.match(/^\d+$/)) || occupancy.red.max < occupancy.red.min)
                        {
                            occupancy.red.max = occupancy.red.min;
                        }

                        // Make sure its an integer
                        occupancy.red.max = parseInt(occupancy.red.max);

                        // Update yellow min value
                        occupancy.yellow.min = occupancy.red.max + 1;

                        // Update yellow max value
                        if (occupancy.yellow.max < occupancy.yellow.min)
                        {
                            occupancy.yellow.max = occupancy.yellow.min;
                        }

                        // Update green min value
                        occupancy.green.min = occupancy.yellow.max + 1;

                        // Update green max value
                        if (occupancy.green.max < occupancy.green.min)
                        {
                            occupancy.green.max = occupancy.green.min;
                        }

                        break;
                    }

                    case 'YELLOW':
                    {
                        // Check if input is empty or contains letters or is smaller than allowed
                        if((occupancy.yellow.max.match && !occupancy.yellow.max.match(/^\d+$/)) || occupancy.yellow.max < occupancy.yellow.min)
                        {
                            occupancy.yellow.max = occupancy.yellow.min;
                        }

                        // Make sure its an integer
                        occupancy.yellow.max = parseInt(occupancy.yellow.max);

                        // Update green min value
                        occupancy.green.min = occupancy.yellow.max + 1;

                        // Update green max value
                        if (occupancy.green.max < occupancy.green.min)
                        {
                            occupancy.green.max = occupancy.green.min;
                        }

                        break;  
                    }

                    case 'GREEN':
                    {
                        // Check if input is empty or contains letters or is smaller than allowed
                        if((occupancy.green.max.match && !occupancy.green.max.match(/^\d+$/)) || occupancy.green.max < occupancy.green.min)
                        {
                            occupancy.green.max = occupancy.green.min;
                        }

                        // Make sure its an integer
                        occupancy.green.max = parseInt(occupancy.green.max);
                    }
                }

                Occupancy.updateOccupancy(occupancy);
            };


            $scope.updateOccupancy = function(occupancy){

                Occupancy.updateOccupancy(occupancy);

                // Write logfile
                Logfile.logChange(occupancy);
            };
        })
})();