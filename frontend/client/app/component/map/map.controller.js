(function () {
    'use strict';

    angular.module('app.map')
        .controller('MapCtrl', function ($scope, $filter, $http, $interval, $mdToast, $mdDialog, Hospital, Occupancy, Comment) {

            var hospitalMarkers = {};
            var subs = [];

            $scope.onMapInitialized = function(map)
            {
                // Try HTML5 geolocation
                if (navigator.geolocation) 
                {
                    navigator.geolocation.getCurrentPosition(function(position) {
                        var pos = {
                          lat: position.coords.latitude,
                          lng: position.coords.longitude
                        };

                        map.setCenter(pos);
                        map.setZoom(12);

                    }, function() {
                        console.log('Could not get location');
                    });
                } 
                else 
                {
                    // Browser doesn't support Geolocation
                    console.log('Browser does not support geo location');
                }

                google.maps.event.addListener(map,'idle', function(){
                            
                    var bounds = map.getBounds();
                    Hospital.listByBounds(bounds).then(function(hospitals) {

                        // Remove previous subscriptions
                        subs.forEach(function(sub, index){
                            Occupancy.Unsubscribe(sub.token);
                        });
                        subs = [];
                        
                        // Draw markers for all visible hospitals and subscribe to changes of those hospitals
                        $scope.hospitals = hospitals;
                        $scope.hospitals.forEach(function(hospital){

                            var sub = Occupancy.SubOccupancy(hospital);
                            sub.promise.then(null, null, function(update) {
                                
                                if (update.objectID !== undefined)
                                {
                                    hospitalMarkers[hospital.id].update();
                                }
                            });
                            subs.push(sub);

                            if (hospitalMarkers[hospital.id] === undefined)
                            {
                                var myLatLng = new google.maps.LatLng(hospital.location.latitude, hospital.location.longitude);
                                var newMarker = new CustomMarker(myLatLng, map, {hospital: hospital});

                                hospitalMarkers[hospital.id] = newMarker;

                                newMarker.addListener('click', function() {
                                    $scope.openDetails(this.targets, hospital);
                                });

                            }
                        });

                    }, function(error) {
                        console.log(error);
                    });
                });
            }

            // Custom marker class
            function CustomMarker(latlng, map, args) {

                this.latlng = latlng;   
                this.args   = args;   
                this.setMap(map);   
            }

            CustomMarker.prototype = new google.maps.OverlayView();

            CustomMarker.prototype.draw = function() {

                var self = this;
                
                var div = this.div;
                
                if (!div) {
                
                    div = this.div = document.createElement('div');

                    var hospital = self.args.hospital;
                    
                    div.className = 'marker';
                    div.setAttribute('id', 'marker-' + hospital.id);
                    
                    div.style.position = 'absolute';
                    div.style.cursor = 'pointer';
                    div.style.background = 'white';

                    div.innerHTML = makeTemplate(hospital);
                    
                    if (typeof(self.args.marker_id) !== 'undefined') {
                        div.dataset.marker_id = self.args.marker_id;
                    }
                    
                    google.maps.event.addDomListener(div, "click", function(event) {            
                        google.maps.event.trigger(self, "click");
                    });
                    
                    var panes = this.getPanes();
                    panes.overlayImage.appendChild(div);
                }
                
                var point = this.getProjection().fromLatLngToDivPixel(this.latlng);
                
                if (point) {
                    div.style.left = point.x - 50 + 'px';
                    div.style.top = point.y - 100 + 'px';
                }
            };

            CustomMarker.prototype.update = function(){

                var panes = this.getPanes();
                panes.overlayImage.lastChild.innerHTML = makeTemplate(this.args.hospital);
            };

            CustomMarker.prototype.remove = function() {

                if (this.div) 
                {
                    this.div.parentNode.removeChild(this.div);
                    this.div = null;
                    this.setMap(null);
                }   
            };

            CustomMarker.prototype.getPosition = function() {
                return this.latlng; 
            };

            function makeTemplate(hospital){

                var wardRows = '';
                hospital.wards.forEach(function(ward){

                    var occupancies = '';
                    ward.occupancies.forEach(function(occupancy){

                        var className = '';
                        if (occupancy.occupancy <= occupancy.red.max)
                            className = 'circle red';
                        else if (occupancy.occupancy >= occupancy.yellow.min && occupancy.occupancy <= occupancy.yellow.max)
                            className = 'circle yellow';
                        else if (occupancy.occupancy >= occupancy.green.min)
                            className = 'circle green';   

                        occupancies += `
                            <div class="col-md-3">
                                <div class="bedtype-wrapper">
                                    <div class="${className}"></div>
                                </div>
                            </div>
                        `;
                    });

                    wardRows += `
                        <div class="row ward">
                            <div class="col-md-3 ward-type">${$filter('translate')(ward.wardType + '_SHORT')}</div>
                            ${occupancies}
                        </div>
                    `;
                });

                var template = `
                    <h2>${hospital.name}</h2>
                    <div class="row ward">
                        <div class="col-md-3"></div>
                        <div class="col-md-3 header">${$filter('translate')('BED_TYPE_SHORT_0')}</div>
                        <div class="col-md-3 header">${$filter('translate')('BED_TYPE_SHORT_1')}</div>
                        <div class="col-md-3 header">${$filter('translate')('BED_TYPE_SHORT_2')}</div>
                    </div>        
                    ${wardRows}
                `;

                return template;
            };

            $scope.openDetails = function(event, hospital){
                
                $mdDialog.show({
                    controller: DialogController,
                    templateUrl: $scope.templateBase + 'map/popover-hospital-details.html',
                    parent: angular.element(document.body),
                    targetEvent: event,
                    clickOutsideToClose: true,
                    fullscreen: $scope.customFullscreen, // Only for -xs, -sm breakpoints.
                    locals: {hospital : hospital},
                });
            };

            function DialogController($scope, $mdDialog, locals){

                $scope.hospital = locals.hospital;
                $scope.newComment = '';

                $scope.hide = function() {
                    $mdDialog.hide();
                };

                $scope.cancel = function() {
                    $mdDialog.hide();
                };

                $scope.saveComment = function() {

                    if ($scope.newComment !== '')
                    {
                        $scope.newComment;

                        var newComment = new Comment();
                        newComment.comment  = $scope.newComment;
                        newComment.user     = Parse.User.current();
                        newComment.hospital = locals.hospital;

                        // Save new comment
                        newComment.save().then(function(comment){

                            $mdDialog.hide();

                        }, function(response, error){
                            console.log(error);
                        });
                    }

                    else
                    {
                        return false;
                    }
                };
            }
    });
})();
