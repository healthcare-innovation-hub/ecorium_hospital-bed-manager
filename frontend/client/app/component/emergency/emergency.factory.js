(function () {
    'use strict';

    angular.module('app.emergency').
    factory('Emergency',  function($q, AuthService, ParseHelperService) {
        var ParseLiveQueryManager = ParseHelperService.CreateLiveQueryManager();

        var Emergency = Parse.Object.extend("Emergency", {
            // Instance methods
            Cancel : function() {
                return Parse.Cloud.run("cancelEmergency", { emergencyId: this.id});
            },
            Finish : function() {
                return Parse.Cloud.run("finishEmergency", { emergencyId: this.id});
            }
        }, {
            // Rest-API Methods
            GetEmergency : function(emergencyID) {
                var defer = $q.defer();

                var query = CreateQuery();
                query.equalTo("objectId", emergencyID);
                query.find({
                    success : function(list) {
                        defer.resolve(list.length > 0 ? list[0] : null);
                    },
                    error : function(aError) {
                        defer.reject(aError);
                    }
                });

                return defer.promise;
            },
            listEmergencies : function() {
                var defer = $q.defer();
                var query = CreateQuery()
                query.find({
                    success : function(emergencyList) {
                        defer.resolve(emergencyList);
                    },
                    error : function(aError) {
                        defer.reject(aError);
                    }
                });

                return defer.promise;
            },

            listEmergenciesByState : function(state) {
                var defer = $q.defer();
                var query = CreateQuery()
                query.equalTo("state", state)
                query.find({
                    success : function(list) {
                        defer.resolve(list);
                    },
                    error : function(error) {
                        defer.reject(error);
                    }
                });

                return defer.promise;
            },

            //LiveQuery Methods
            SubActiveEmergencies : function() {
                var queryID = "SubActiveEmergencies";
                var result = ParseLiveQueryManager.SubscribeToQuery(queryID);
                if(result.createSubscription)
                {
                    var query = CreateQuery()
                    query.descending("createdAt");

                    query.lessThan("state", 20);
                    ParseLiveQueryManager.CreateSubscription(queryID, query)
                }
                return result.retObject;
            },

            SubFinishedEmergencies : function() {
                var queryID = "SubFinishedEmergencies";
                var result = ParseLiveQueryManager.SubscribeToQuery(queryID);
                if(result.createSubscription)
                {
                    var query = CreateQuery()
                    query.descending("createdAt");

                    query.equalTo("state", 20);
                    ParseLiveQueryManager.CreateSubscription(queryID, query)
                }
                return result.retObject;
            },

            SubEmergency : function(emergencyID) {
                var queryID = "SubEmergency"+emergencyID;
                var result = ParseLiveQueryManager.SubscribeToQuery(queryID);
                if(result.createSubscription)
                {
                    var query = CreateQuery();
                    query.equalTo("objectId", emergencyID)
                    ParseLiveQueryManager.CreateSubscription(queryID, query)
                }
                return result.retObject;
            },

            Unsubscribe : function(subToken) {
                ParseLiveQueryManager.RemoveSubscription(subToken);
            }
        });

        function CreateQuery(){
            var query = new Parse.Query(Emergency);
            query.exists("controlCenterRelation");
            query.equalTo("controlCenterRelation", Parse.User.current().get("controlCenterRelation"));
            return query;
        }

        ParseHelperService.patchBindings(Emergency, ["informationString", "objectName", "indicatorName", "keyword", "locationPoint", "state", "patientName", "city", "streetNumber", "streetName", "zip", "country", "emergencyNumber", "NEFArrivalDate"]);
        return Emergency;
    });
})();
