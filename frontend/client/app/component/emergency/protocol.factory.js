(function () {
    'use strict';

    angular.module('app.emergency').
    factory('Protocol',  function($q, ParseHelperService) {
        var ParseLiveQueryManager = ParseHelperService.CreateLiveQueryManager();

        var Protocol = Parse.Object.extend("Protocol", {
            // Instance methods
        }, {
            saveByObject: function(object) {
                var defer = $q.defer();
                object.save(null, {
                    success : function(aEmergency) {
                        defer.resolve(aEmergency);
                    },
                    error : function(aError) {
                        defer.reject(aError);
                    }
                });

                return defer.promise;
            },
            // Rest-API Methods
            listById : function(emergencyID) {
                var defer = $q.defer();

                var query = new Parse.Query(this);
                query.equalTo("objectId", emergencyID);
                query.find({
                    success : function(aEmergency) {
                        defer.resolve(aEmergency);
                    },
                    error : function(aError) {
                        defer.reject(aError);
                    }
                });

                return defer.promise;
            },
            listProtocolsByState : function(state) {
                var defer = $q.defer();
                var query = new Parse.Query(this);
                query.equalTo("state", state)
                query.find({
                    success : function(list) {
                        defer.resolve(list);
                    },
                    error : function(error) {
                        defer.reject(error);
                    }
                });
                return defer.promise;
            },

            SubProtocol : function(protocolID) {
                var queryID = "SubProtocol"+protocolID;
                var result = ParseLiveQueryManager.SubscribeToQuery(queryID);
                if(result.createSubscription)
                {
                    var query = new Parse.Query("Protocol");
                    query.equalTo("objectId", protocolID)
                    ParseLiveQueryManager.CreateSubscription(queryID, query)
                }
                return result.retObject;
            },

            Unsubscribe : function(subToken) {
                ParseLiveQueryManager.RemoveSubscription(subToken);
            }
        });

        ParseHelperService.patchBindings(Protocol, ["userbasedDataRemoved", "measureDefiPhase", "measureDefiPulseCount", "endRespirationValue", "measureDefiValue", "measureDefiShockCount", "endReactionValue", "startLocationValue", "handoverRespiration", "handoverPulse","coreAction", "startDiagnoseValue", "handoverConsciousness", "emergencyLocation", "supposedCause", "measureDefiSuccess", "cancelComment", "createdAt", "done", "endComment", "endStatus", "measureBreathDonationValue", "measureChestCompressionValue", "measureMeasureValue", "objectId", "startLaterRelAmbulance", "startMinutesRelAmbulance", "startOrientationValue", "startPupil", "startPupilLeft", "startPupilRight", "startReactionValue", "startRespirationValue", "updatedAt"]);
        return Protocol;
    });
})();

