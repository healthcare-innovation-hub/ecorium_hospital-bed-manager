(function () {
    'use strict';
    angular.module('app.emergency').
    factory('LocationTracking',  function($q, ParseHelperService, EmergencyState) {
        var ParseLiveQueryManager = ParseHelperService.CreateLiveQueryManager();

        var LocationTracking = Parse.Object.extend("LocationTracking", {
        }, {
            //LiveQuery Methods
            SubLocationTrackingByEmergencyState : function(emergencyStateID) {
                var queryID = "SubLocationTracking_" + emergencyStateID;
                var result = ParseLiveQueryManager.SubscribeToQuery(queryID);
                if(result.createSubscription)
                {
                    var query = new Parse.Query(LocationTracking);
                    var emergencyState = new EmergencyState();
                    emergencyState.id = emergencyStateID;
                    query.equalTo("emergencyStateRelation", emergencyState)
                    query.descending("createdAt");
                    ParseLiveQueryManager.CreateSubscription(queryID, query)
                }
                return result.retObject;
            },

            Unsubscribe : function(subToken) {
                ParseLiveQueryManager.RemoveSubscription(subToken);
            }
        });
        ParseHelperService.patchBindings(LocationTracking, ["location", "duration", "distance"]);
        return LocationTracking;
    });
})();
