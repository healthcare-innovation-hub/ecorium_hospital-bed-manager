(function () {
    'use strict';

    angular.module('app.emergency')
        .controller('EmergencyListCtrl',function ($scope, $timeout, $rootScope, Emergency) {
        
        $scope.loadData = function () {
            $rootScope.$broadcast('preloader:active');
            Emergency.listEmergenciesByState(20).then(function(emergencies) {
                $scope.emergencies = emergencies;
                $rootScope.$broadcast('preloader:hide');
            }, function(aError) {
                // Something went wrong, handle the error
            });
        };
        $scope.loadData();
    })
})();
