(function () {
    'use strict';

    

    angular.module('app.emergency').
    factory('EmergencyState',  function($q, ParseHelperService, Emergency, Firstresponder) {
        var ParseLiveQueryManager = ParseHelperService.CreateLiveQueryManager();

        var EmergencyState = Parse.Object.extend("EmergencyState", {
            // Instance methods
            Cancel : function() {
                return Parse.Cloud.run("cancelFirstResponder", { emergencyStateId: this.id});
            },

            PushMessage : function(title, message){
                return Parse.Cloud.run("pushToFirstresponder", { installationId: this.installationRelation.id, alert: title, message: message, state: 2});
            }
        }, {
            // Rest-API Methods
            ListEmergencyStates: function(emergencyID){
                var defer = $q.defer();
                var query = new Parse.Query(this);
                var emergency = new Emergency();
                emergency.id = emergencyID;
                query.equalTo("emergencyRelation", emergency)
                query.include("protocolRelation")
                query.include("userRelation")

                query.find({
                    success : function(list) {
                        defer.resolve(list);
                    },
                    error : function(error) {
                        defer.reject(error);
                    }
                });
                return defer.promise;                
            },
            //LiveQuery Methods
            SubEmergencyStates : function(emergencyID) {
                var queryID = "SubEmergencyStates_"+emergencyID;
                var result = ParseLiveQueryManager.SubscribeToQuery(queryID);
                if(result.createSubscription)
                {
                    var query = new Parse.Query("EmergencyState");
                    var emergency = new Emergency();
                    emergency.id = emergencyID;
                    query.equalTo("emergencyRelation", emergency)
                    query.include("userRelation")
                    ParseLiveQueryManager.CreateSubscription(queryID, query)
                }
                return result.retObject;
            },

            Unsubscribe : function(subToken) {
                ParseLiveQueryManager.RemoveSubscription(subToken);
            }
        });
        ParseHelperService.patchBindings(EmergencyState, ["acceptedAt", "arrivedAt", "calledBackAt", "cancelledAt", "contactedAt", "createdAt", "emergencyRelation", "endedAt", "installationRelation", "maxRadius", "objectId", "protocolRelation", "readyAt", "state", "updatedAt", "userProfession", "userQualification", "userRelation"]);
        return EmergencyState;
    });
})();
