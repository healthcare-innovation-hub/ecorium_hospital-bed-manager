(function () {
    'use strict';

    angular.module('app.emergency')
        .controller('EmergencyCtrl', function ($scope, $http, $interval, $mdToast, $mdDialog, Emergency) {

        var initializedMapView = false;
        var sub = Emergency.SubActiveEmergencies();
        console.log(sub);
        sub.promise.then(null, null, function(update) {
            $scope.emergencies = update.state;
            $scope.count = Object.keys($scope.emergencies).length;
            if($scope.map != undefined)
                InitializeMapView($scope);
        })

        $scope.emergencies = sub.initstate;
        $scope.count = Object.keys($scope.emergencies).length;
        $scope.highlightedEmergency = null;

        $scope.onMapInitialized = function(map)
        {
            $scope.map = map;
            google.maps.event.trigger(map, "resize");
            InitializeMapView($scope);
        }.bind(this);

        $scope.highlight = function(event, emergency, highlighted)
        {
            $scope.highlightedEmergency = (highlighted) ? emergency.id : null;
        }.bind(this);

        $scope.GetIconOrigin = function(type)
        {
            if(type == 'highlighted')
                return [37,0]
            else if(type == 'emergency')
                return [74,0]
            else if(type == 'emergency-highlighted')
                return [111,0]
            else
                return [0,0]
        }.bind(this);

        $scope.CancelEmergency = function(emergency){
            emergency.Cancel().then(function(result) {
                $mdToast.show(
                    $mdToast.simple()
                        .content('Notfall wurde abgebrochen.')
                        .position("bottom right")
                        .hideDelay(1500)
                );
            }, function(error) {
                    console.warn(error);
            });
        }

        $scope.FinishEmergency = function(emergency){
            emergency.Finish().then(function(result) {
                if(result == 21) {
                    $mdDialog.show(
                        $mdDialog.alert()
                            .parent(angular.element(document.querySelector('#popupContainer')))
                            .clickOutsideToClose(true)
                            .title('Notfall ' + emergency.emergencyNumber + ' beendet')
                            .content('An dem Notfall mit der Nummer ' + emergency.emergencyNumber + ' waren keine Ersthelfer beteiligt. Er ist hiermit abgeschlossen.')
                            .ok('Bestätigen')
                    );
                } else {
                    $mdToast.show(
                        $mdToast.simple()
                            .content('Notfall wurde beendet.')
                            .position("bottom right")
                            .hideDelay(1500)
                    );
                }

            }, function(error) {
                    console.warn(error);
            });
        }

        $scope.$on("$destroy", function handler() {
            Emergency.Unsubscribe(sub.token);
        });
    })

    function CheckEmergencyState(emergency) {
        if(emergency && emergency.state >= 20)
        {
            $mdDialog.show(
                $mdDialog.alert()
                    .parent(angular.element(document.querySelector('#popupContainer')))
                    .clickOutsideToClose(true)
                    .title('Notfall ' + emergency.emergencyNumber + ' beendet')
                    .content('Der Notfall mit der Nummer ' + emergency.emergencyNumber + ' wurde von einem Leitstellenmitarbeiter beendet')
                    .ok('Bestätigen')
            );
        }
    }

    function InitializeMapView($scope) {
        if ($scope.count > 0) {
            var bounds = new google.maps.LatLngBounds();
            for (var objectID in $scope.emergencies) {
                if ($scope.emergencies[objectID].locationPoint != undefined)
                    bounds.extend(new google.maps.LatLng($scope.emergencies[objectID].locationPoint.latitude, $scope.emergencies[objectID].locationPoint.longitude));
            }
            $scope.map.fitBounds(bounds);
        }
    }
})();
