(function () {
    'use strict';

    angular.module('app')
        .factory('AuthService', function ($q, $rootScope, $cookies, ControlCenter) {
            var initialized = false;
            var roles = new Set();
            var roleNames = new Set();
            var mainRole = null;
            var privileges = new Set();
            var superuser = false;

            var startPage = '';

            function getRolesRecursively(roleArray) {
                var resultRoles = roleArray;
                var q = $q.defer();
                if (roleArray.length == 0)
                    q.resolve([]);
                else {
                    roleArray.forEach(function (role) {
                        role.getRoles().query().find().then(function (roles) {
                            resultRoles = resultRoles.concat(roles);
                            getRolesRecursively(roles).then(function (roles) {
                                q.resolve(resultRoles.concat(roles));
                            });
                        })
                    })
                }
                return q.promise;
            }

            return {
                isInitialized: function () {
                    return initialized;
                },
                
                initialize: function () {
                    console.assert(!this.isInitialized(), "AuthService was already initialized!")

                    var _this = this;
                    roles.clear();
                    roleNames.clear();
                    privileges.clear();
                    mainRole = null;
                    superuser = false;
                    
                    var defer = $q.defer();
                    if ((Parse.User.current() !== null)) {
                        var query = new Parse.Query(Parse.Role);
                        query.equalTo('users', Parse.User.current());
                        query.find().then(
                            function (fetchedRoles) {
                                mainRole = (fetchedRoles.length > 0) ? fetchedRoles[0] : null;
                                getRolesRecursively(fetchedRoles).then(function (fetchedRoles) {
                                    for (var role of fetchedRoles) {
                                        roles.add(role);
                                        roleNames.add(role.get("name"))
                                        for(var privilege of role.get("privileges"))
                                            privileges.add(privilege);
                                    }
                                    initialized = true;
                                    superuser = _this.hasPrivilege("su.any");
                                    if(_this.hasPrivilege("su.CCSwitch") && $cookies.getObject("su.CCSwitchId") != undefined) {
                                        ControlCenter.getById($cookies.getObject("su.CCSwitchId")).then(function(controlCenter){
                                            Parse.User.current().set("controlCenterRelation", controlCenter);
                                            defer.resolve();
                                        })
                                    }
                                    else
                                        defer.resolve();
                                })
                            },
                            function (e) {
                                console.warn("Error on role request:" + e);
                            }
                        );
                    } else {
                        initialized = true;
                        defer.resolve();
                    }
                    return defer.promise;
                },

                hasPrivilege: function (privilege) {
                    console.assert(this.isInitialized(), "AuthService was not yet initialized!")
                    return superuser ? true : privileges.has(privilege)
                },

                hasRolex: function (role) {
                    console.assert(this.isInitialized(), "AuthService was not yet initialized!")
                    return superuser ? true : roleNames.has(role)
                },

                getPrivileges: function () {
                    console.assert(this.isInitialized(), "AuthService was not yet initialized!")
                    return privileges;
                },

                getMainRole: function () {
                    console.assert(this.isInitialized(), "AuthService was not yet initialized!")
                    return mainRole;
                },

                getRoles: function () {
                    console.assert(this.isInitialized(), "AuthService was not yet initialized!")
                    return roles;
                },

                getRoleNames: function () {
                    console.assert(this.isInitialized(), "AuthService was not yet initialized!")
                    return roleNames;
                },

                login: function (username, password) {
                    var _this = this;
                    console.assert(this.isInitialized(), "AuthService was not yet initialized!")
                    var defer = $q.defer();
                    Parse.User.logIn(username, password, {
                        success: function (user) {
                            initialized = false;
                            _this.initialize().then(function () {
                                if (_this.getRoles().size > 0)
                                    defer.resolve();
                                else 
                                    defer.reject("Sie sind nicht authorisiert die Seite zu betreten.");
                            });
                        },
                        error: function (error) {
                            console.warn(error);
                            defer.reject("Falscher Benutzername oder Passwort!");
                        }
                    });
                    return defer.promise;
                },

                logout: function () {
                    console.assert(this.isInitialized(), "AuthService was not yet initialized!")
                    $('#emergencyListMap').remove();
                    window.google = {};
                    return Parse.User.logOut();
                },

                isAuthenticated: function () {
                    console.assert(this.isInitialized(), "AuthService was not yet initialized!")
                    return (Parse.User.current() !== null)
                },
            };
        })
})();
