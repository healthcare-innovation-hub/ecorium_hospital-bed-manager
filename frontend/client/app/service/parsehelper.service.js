(function() {
    'use strict';

    angular.module('app')
        .factory('ParseHelperService', function ($q){

            var patchBindings = function(classPrototype, keyArray) {
                keyArray.forEach(function(attrib){
                    classPrototype.prototype.__defineGetter__(attrib, function() {
                        return this.get(attrib);
                    });

                    classPrototype.prototype.__defineSetter__(attrib, function(aValue) {
                        return this.set(attrib, aValue);
                    });
                });
            };

            var patchSubclass = function(subclassObject) {
                const jsonObject = subclassObject.toJSON();
                jsonObject.className = subclassObject.className;
                return Parse.Object.fromJSON(jsonObject);
            };

            var CreateLiveQueryManager = function() {
                return new LiveQueryManager(this, $q);
            };

            return {
                patchBindings: patchBindings,
                patchSubclass : patchSubclass,
                CreateLiveQueryManager : CreateLiveQueryManager
            };
        });

        function LiveQueryManager(ParseHelperService, $q){
            var subscriptions = {};
            function SubscribeToQuery(queryID)
            {
                var createSubscription = !subscriptions.hasOwnProperty(queryID);
                if(createSubscription)
                    subscriptions[queryID] = {defer: $q.defer(), subTokens: [], subTokenIndex:0, state: {}}; 

                var subObj = subscriptions[queryID];
                var subToken = queryID + ":" + (++subObj.subTokenIndex);
                subObj.subTokens.push(subToken);
                return { retObject: {token: subToken, promise: subObj.defer.promise, initstate: subObj.state }, createSubscription: createSubscription};
            }

            function CreateSubscription(queryID, query)
            {
                query.find({
                    success : function(objectList) {
                        if(subscriptions.hasOwnProperty(queryID))
                        {
                            var subObj = subscriptions[queryID];
                            var defer = subObj.defer;
                            for(var indexOfObjects = 0; indexOfObjects < objectList.length; ++indexOfObjects)
                                subObj.state[objectList[indexOfObjects].id] = objectList[indexOfObjects];

                            subObj.subscription = query.subscribe();
                            ["create", "enter", "update"].forEach(function(command) {
                                subObj.subscription.on(command, function (object) {
                                    var objectID = object.id; 
                                    subObj.state[objectID] = ParseHelperService.patchSubclass(object); // subclass patching required because of bug in Parse https://github.com/ParsePlatform/Parse-SDK-JS/issues/372
                                    defer.notify({action: command, state: subObj.state, objectID: objectID})
                                })
                            });

                            ["leave", "delete"].forEach(function(command) {
                                subObj.subscription.on(command, function (object) {
                                    var objectID = object.id; 
                                    delete subObj.state[objectID];
                                    defer.notify({action: command, state: subObj.state, objectID: objectID})
                                })
                            });

                            defer.notify({action: 'init', state: subObj.state})
                        }
                    },
                    error : function(aError) {
                        console.warn(aError);
                    }
                });
            }

            function RemoveSubscription(subToken)
            {
                var queryID      = subToken.split(':')[0];
                var subscription = subscriptions[queryID];

                if (subscription)
                {
                    var index = subscription.subTokens.indexOf(subToken);

                    if(index >= 0)
                    {
                        subscriptions[queryID].subTokens.splice(index, 1);
                        if(subscriptions[queryID].subTokens.length <= 0)
                        {
                            if(subscriptions[queryID].subscription != undefined && typeof subscriptions[queryID].subscription.unsubscribe === "function")
                            {
                                subscriptions[queryID].subscription.unsubscribe();
                            }
                            delete subscriptions[queryID];
                        }
                    }
                }
            }

            return {
                SubscribeToQuery: SubscribeToQuery,
                CreateSubscription : CreateSubscription,
                RemoveSubscription : RemoveSubscription
            };
        }

})();
