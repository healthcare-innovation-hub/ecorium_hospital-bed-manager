# Krankenhaus Bettenmanager

## Idee
Der Belegungsmanager verwaltet Bettenkontingente pro Krankenhaus, sodass Leitstellen einen transparenten Überblick bekommen, welches Krankenhaus für die Aufnahme von Patienten geeignet ist.

## Installation des Frontends
* npm install
* bower install

## Start des Frontends
* gulp serve

## Installation des Backends
* npm install

## Start des Backends
* node index.js
